/*-----------------------------------------------------------------------------
 * @package:    MMOD Panel
 * @author:     Richard B Winters
 * @copyright:  2011-2021 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.3.0
 *---------------------------------------------------------------------------*/


// Deps
const main = imports.ui.main;

const lang = imports.lang;


// Defines
const EDGE_BOTTOM = 0;
const EDGE_TOP = 1;


function mod( o )
{
    if( o )
    {
        if( o.rig )
        {
            this.rig = o.rig;
        }
    }
    else
    {
        this.rig = null;
    }

    this.comfortSettings = null;
    this.box = main.panel.actor.get_parent();
    this.monitor = main.layoutManager.primaryMonitor;

    this.realize = -1;

    this.active = false;
}


mod.prototype.init = function()
{
    // Apply style modifications first so that we get proper the height calculated for bottom positioning
    main.panel._addStyleClassName( this.comfortSettings.containerStyle );

    // Handle realize if applicable
    if( this.realize < 0 )
    {   // Shell/Extension has just initialized
        this.realize = this.box.connect( 'realize', lang.bind( this, this.init ) );

        // Do not allow the method to continue
        return;
    }

    if( this.realize > 0 )
    {   // This is the second invocation, called after realization of the panelBox
        this.box.disconnect( this.realize );
        this.realize = 0;
    }

    // Handle the panel location mod:
    main.panel.edge = this.rig.settings.get_enum( 'panel-position' );
    if( !main.panel.edge ) // EDGE_BOTTOM:
    {   // Try to dynamically handle positioning based on height:
        let boxHeight = this.box.get_height();
        if( !boxHeight )
        {   // Fall back to hard-coded heights if necessary:
            switch( this.comfortSettings.containerStyle )
            {
                case 'mmod-panel-style-compact':
                    boxHeight = 28;
                    break;
                case 'mmod-panel-style-comfortable':
                    boxHeight = 52;
                    break;
                default:    // 'mmod-panel-style-cozy':
                    boxHeight = 36;
            }
        }

        this.box.set_y( this.monitor.height - boxHeight );
    }
    else
    {   // EDGE_TOP:
        this.box.set_y( this.monitor.y );
    }

    this.active = true;
};


mod.prototype.destroy = function()
{
    if( this.active )
    {
        // Replace the panelBox to its rightful position
        this.box.set_y( this.monitor.y );

        // Remove styling over-ride applied to the panel
        main.panel._removeStyleClassName( this.comfortSettings.containerStyle );

        // Delete our custom property inside of Main.panel
        delete main.panel.edge;
        this.active = false;
    }

    this.realize = 0;
};
